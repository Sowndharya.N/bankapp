REM   Script: Bank App
REM   SQL Hands on 

Create table Customer ( 
    Customer_id int primary key auto increment, 
    Customer_name varchar(20), 
    dob date, 
    Gender varchar(10), 
    City varchar(20) 
);

Create table Customer ( 
    Customer_id int primary key, 
    Customer_name varchar(20), 
    dob date, 
    Gender varchar(10), 
    City varchar(20) 
);

Create table Customer (  
    Customer_id int primary key,  
    Customer_name varchar(20),  
    dob date,  
    Gender varchar(10),  
    City varchar(20)  
);

desc customer


Create table Transactions( 
    Transaction_id int primary key, 
    Transaction_type varchar(10) check(Transaction_type in ('CREDIT','DEBIT')), 
    Transaction_Amount int, 
    Transaction_datetime TIMESTAMP WITH LOCAL TIME ZONE DEFAULT LOCALTIMESTAMP, 
    Transaction_from int, 
    Transaction_to int, 
    constraint fk_trans_from foreign key (Transaction_from) references Customer(Customer_id), 
    constraint fk_trans_to foreign key(Transaction_to) references Customer(Customer_id), 
    Transaction_status varchar(20) check(Transaction_status in ('NEW','IN PROGRESS','PROCESSED','PENDING','FAILED','RETRY') 
);

Create table Transactions( 
    Transaction_id int primary key, 
    Transaction_type varchar(10) check(Transaction_type in ('CREDIT','DEBIT')), 
    Transaction_Amount int, 
    Transaction_datetime TIMESTAMP WITH LOCAL TIME ZONE DEFAULT LOCALTIMESTAMP, 
    Transaction_from int, 
    Transaction_to int, 
    constraint fk_trans_from foreign key (Transaction_from) references Customer(Customer_id), 
    constraint fk_trans_to foreign key(Transaction_to) references Customer(Customer_id), 
    Transaction_status varchar(20) check(Transaction_status in ('NEW','IN PROGRESS','PROCESSED','PENDING','FAILED','RETRY')) 
);

desc Transaction


desc Transactions


Create table Customer (  
    Customer_id int primary key,  
    Customer_name varchar(20),  
    dob date,  
    Gender varchar(10),  
    City varchar(20)  
);

desc customer


Create table Transactions( 
    Transaction_id int primary key, 
    Transaction_type varchar(10) check(Transaction_type in ('CREDIT','DEBIT')), 
    Transaction_Amount int, 
    Transaction_datetime TIMESTAMP WITH LOCAL TIME ZONE DEFAULT LOCALTIMESTAMP, 
    Transaction_from int, 
    Transaction_to int, 
    constraint fk_trans_from foreign key (Transaction_from) references Customer(Customer_id), 
    constraint fk_trans_to foreign key(Transaction_to) references Customer(Customer_id), 
    Transaction_status varchar(20) check(Transaction_status in ('NEW','IN PROGRESS','PROCESSED','PENDING','FAILED','RETRY')) 
);

desc Transactions


insert into Customer values(1,'Sowndharya','06-Aug-1998','Female','Coimbatore');

insert into Customer values(1,'Sowndharya','06-Aug-1998','Female','Coimbatore');

insert into Customer values(1,'Sowndharya','06-Aug-1998','Female','Coimbatore');

insert into Customer values(1,'Sowndharya','06-Aug-1998','Female','Coimbatore');

insert into Customer values(1,'Sowndharya','06-Aug-1998','Female','Coimbatore');

insert into Customer values(1,'Sowndharya','06-Aug-1998','Female','Coimbatore');

insert into Customer values(2,'Hussain','11-Apr-2006','Male','Coimbatore');

insert into Customer values(3,'Abdul','21-Apr-1998','Male','Madurai');

insert into Customer values(4,'Pushpa','13-Nov-1980','Female','Chennai');

insert into Customer values(5,'Vignesh','18-May-1999','Male','Trippur');

insert into Customer values(6,'Arief','01-Jan-1970','Male','Arakkonam');

insert into Customer values(7,'Selvi','27-Apr-1995','Female','Virudhunagar');

insert into Customer values(8,'Ajeetha','15-Oct-2000','Female','Dingigul');

insert into Customer values(1,'Sowndharya','06-Aug-1998','Female','Coimbatore');

select * from Customer;

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(1,'CREDIT',200,1,8,'NEW');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(2,'CREDIT',150,2,7,'IN PROGRESS');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(3,'CREDIT',300,3,6,'PROCESSED');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(4,'CREDIT',400,4,5,'PENDING');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(5,'CREDIT',200,7,2,'FAILED');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(6,'CREDIT',100,8,1,'RETRY');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(7,'CREDIT',250,5,6,'NEW');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(8,'CREDIT',200,1,8,'IN PROGRESS');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(9,'DEBIT',2000,1,1,'PROCESSED');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(10,'DEBIT',2500,2,2,'PENDING');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(11,'DEBIT',1000,3,3,'FAILED');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(12,'DEBIT',3000,4,4,'RETRY');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(13,'DEBIT',2000,5,5,'NEW');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(14,'DEBIT',5000,6,6,'PROCESSED');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(15,'DEBIT',6000,7,7,'IN PROGRESS');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(16,'DEBIT',7000,8,8,'RETRY');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(17,'CREDIT',1000,2,3,'NEW');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(18,'CREDIT',1500,8,4,'IN PROGRESS');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(19,'CREDIT',3000,5,1,'PROCESSED');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(20,'CREDIT',800,4,8,'PENDING');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(21,'CREDIT',3500,2,6,'FAILED');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(22,'CREDIT',400,4,3,'RETRY');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(23,'CREDIT',4000,6,5,'NEW');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(24,'CREDIT',7500,2,8,'IN PROGRESS');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(25,'DEBIT',2300,1,1,'PROCESSED');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(26,'DEBIT',7200,2,2,'PENDING');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(27,'DEBIT',1800,3,3,'FAILED');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(28,'DEBIT',9000,4,4,'RETRY');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(29,'DEBIT',3000,5,5,'NEW');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(30,'DEBIT',6500,6,6,'PROCESSED');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(31,'DEBIT',7700,7,7,'IN PROGRESS');

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(32,'DEBIT',3900,8,8,'RETRY');

select * from Transactions;

select * from (select Customer_id, Customer_name, Gender, City, trunc(months_between(sysdate,dob/12)) as "Age" from Customer) 
where "Age">18 and "Age"<32;

select * from (select Customer_id, Customer_name, Gender, City, trunc(months_between(sysdate,dob)/12) as "age" from Customer) 
where "age">18 and "age"<32;

select City from Customer group by City having count(City)=1;

select * from Customer where City in (select City from Customer group by City having count(City)>1);

select City from Customer group by City having count(City)=1;

select * from Transactions where Transaction_Amount<250;

select * from Transactions where Transaction_Amount>500 and Transaction_type='CREDIT';

select * from Transactions where Transaction_status='PROGRESS';

select * from Transactions where Transaction_status='IN PROGRESS';

select * from Transactions where Transaction_status='PENDING' and Transaction_status='RETRY';

select * from Transactions where Transaction_status='PENDING' or Transaction_status='RETRY';

select * from Transactions where Transaction_status='NEW' and Transaction_type='DEBIT';

select c.*, t.* from Customer c inner join Transactions t on t.Transactions_from = c.Customer_id  
and t.Transactions_type='DEBIT' where c.City='Coimbatore';

select c.*, t.* from Customer c inner join Transactions t on t.Transactions_from = c.Customer_id  
and t.Transaction_type='DEBIT' where c.City='Coimbatore';

select c.*, t.* from Customer c inner join Transactions t on t.Transaction_from = c.Customer_id  
and t.Transaction_type='DEBIT' where c.City='Coimbatore';

select c.*,t.* from Customer c inner join Transactions t on t.Transaction_from = c.Customer_id 
and t.Transaction_status='FAILED' where trunc (months_between(sysdate,c.dob)/12)>30;

select c.*, t.* from Customer c inner join Transactions t on t.Transaction_from = c.Customer_id 
and t.Transaction_status='FAILED' where trunc (months_between(sysdate,c.dob)/12)>30;

insert into Transactions (Transaction_id,Transaction_type,Transaction_Amount,Transaction_from,Transaction_to,Transaction_status)  
values(33,'DEBIT',2800,6,6,'FAILED');

select c.*, t.* from Customer c inner join Transactions t on t.Transaction_from = c.Customer_id 
and t.Transaction_status='FAILED' where trunc (months_between(sysdate,c.dob)/12)>30;

select c.*, t.* from Customer c inner join Transactions t on t.Transaction_from = c.Customer_id 
and c.Gender='Female' and t.Transaction_type='DEBIT' where Transaction_status='NEW' or Transaction_status='RETRY';

